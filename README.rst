DivinityBot
===========

`Calibrates`__, bringing you closer to Divine Omniscience™.

__ https://www.lesswrong.com/posts/225697eyttttp6pBj/a-note-about-calibration-of-confidence

How it should work
------------------

1) You connect to this bot
2) It asks you to select a subset of questions you wish to
   receive from a list.
3) Every morning it asks you three questions, and you should make your
   predictions.
4) During the day you can commit the actual answers to the questions
   (bot should remind you at evening if you forgot to commit a
   question).
5) Every few days it will display you your calibration status and how
   are you doing compared to other participants.
