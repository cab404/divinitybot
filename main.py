#  This file is part of DivinityBot.
#
#  DivinityBot is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  DivinityBot is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with DivinityBot.  If not, see <https://www.gnu.org/licenses/>.

from logging import DEBUG

from divine import tg
from asyncio import run, gather

import logging

from divine.questions import binary_question, new_binary_question
from divine.tg_models import MessageText, QuestionSet, QuestionConfig
from divine.tg import TelegramConfig, StorageConfig

import toml

logging.basicConfig(level=DEBUG)


def fset(*args):
    return frozenset(args)


q_config = QuestionConfig(
    questions_sets={
        "test-questions": QuestionSet(
            title="Тестовые вопросы",
            questions={
                "see-sepulka": binary_question(
                    text=MessageText("Сегодня я увижу сепульку.")
                ),
                "not-bad": binary_question(
                    text=MessageText("Мой день будет неплохим.")
                ),
                "nonrandom-coin": binary_question(
                    text=MessageText("Я подброшу монетку, и выпадет орел.")
                ),
                "weather-oracle": binary_question(
                    text=MessageText("Сегодня будет дождь.")
                ),
                "weather-oracle2": new_binary_question(
                    text=MessageText("Сегодня будет сильный дождь!.")
                ),
            },
        )
    }
)

with open("config.toml", "r") as f:
    toml_cfg = toml.load(f)

tg_config = TelegramConfig(
    storage=StorageConfig(redis_params={"address": ("localhost", 6379)}),
    questions=q_config,
    **toml_cfg
)

run(tg.create_telegram_bot(tg_config))
