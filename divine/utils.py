#  This file is part of DivinityBot.
#
#  DivinityBot is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  DivinityBot is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with DivinityBot.  If not, see <https://www.gnu.org/licenses/>.
from typing import Union, NamedTuple

import aiofiles
import jinja2


async def read_template(name: str):
    async with aiofiles.open(name) as f:
        return jinja2.Template(await f.read(), enable_async=True)


def merge(*args: "Union[dict, NamedTuple]", **kwargs):
    # TODO: Replace with lazy mappings?
    merged = {}
    for m in args:
        # noinspection PyProtectedMember
        merged.update(m._asdict() if getattr(m, "_asdict", None) else m)
    merged.update(**kwargs)
    return merged


def brier(answers: List[Tuple[float, bool]]):
    return sum(
        pow(certainty - 1 if correct else 0, 2) for (certainty, correct) in answers
    ) / len(answers)
