#  This file is part of DivinityBot.
#
#  DivinityBot is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  DivinityBot is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with DivinityBot.  If not, see <https://www.gnu.org/licenses/>.

import json
import typing
from asyncio import gather
from typing import (
    AnyStr,
    Any,
    Callable,
    Coroutine,
    Tuple,
    TypeVar,
    Awaitable,
    Optional,
    List,
    Set,
    Generic,
)
import logging
from aiogram.types import User
from aioredis import RedisConnection

_St = TypeVar("_St")
_Rt = TypeVar("_Rt")
RdsStr = Optional[AnyStr]

log = logging.getLogger("rds-helper")


async def _a_apply(func: Callable[[_St], _Rt], f: Awaitable[_St]) -> _Rt:
    return func(await f)


_val_conv: Callable[[RdsStr], str] = lambda b: str(b, encoding="utf-8") if b else None
"""Converts some byte string or None to string or None. Useful for reading redis stuff."""


class RedisSet:
    def __init__(self, rds: "RedisConnection", key: str):
        self.rds = rds
        self.key = key

    async def add(self, value: RdsStr):
        return await self.rds.execute("SADD", self.key, value)

    async def items(self) -> Set[RdsStr]:
        return set(_val_conv(m) for m in await self.rds.execute("SMEMBERS", self.key))

    async def contains(self, value: RdsStr) -> bool:
        return await self.rds.execute("SISMEMBER", self.key, value)

    async def remove(self, value: RdsStr):
        return await self.rds.execute("SREM", self.key, value)


class RedisValue(Generic[_St]):
    def __init__(
        self,
        rds: "RedisConnection",
        key: str,
        casts: Tuple[Callable[[_St], str], Callable[[str], _St]] = (
            lambda a: a,
            lambda a: a,
        ),
    ):
        self.casts = casts
        self.rds = rds
        self.key = key

    async def set(self, value: _St):
        value = self.casts[0](value)
        return (
            await self.rds.execute("SET", self.key, value)
            if value
            else self.rds.execute("DEL", self.key)
        )

    async def set_timeout(self, value: _St, timeout_seconds: int):
        value = self.casts[0](value)
        return (
            await self.rds.execute("SETEX", self.key, timeout_seconds, value)
            if value
            else self.rds.execute("DEL", self.key)
        )

    async def get(self) -> _Rt:
        return self.casts[1](_val_conv(await self.rds.execute("GET", self.key)))

    async def ttl(self) -> Optional[int]:
        time = int(await self.rds.execute("TTL", self.key))
        if time == -1:
            return None
        if time == -2:
            return 0
        return time


class RedisHelper:
    def __init__(self, redis: RedisConnection, key_mapper: Callable[[str], str]):
        self.key_mapper = key_mapper
        self.redis = redis
        _key_conv: Callable[[int], str]

    def get_value(self, key: AnyStr, **kvargs) -> RedisValue:
        return RedisValue(self.redis, self.key_mapper(key), **kvargs)

    def get_set(self, key: AnyStr) -> RedisSet:
        return RedisSet(self.redis, self.key_mapper(key))

    def stack(self, new_mapper: Callable[[str], str]):
        return RedisHelper(
            redis=self.redis, key_mapper=lambda a: self.key_mapper(new_mapper(a))
        )


class MessageValue:
    def __init__(self, helper: "RedisHelper", m_id: int) -> None:
        super().__init__()
        helper = helper.stack(lambda a: f"message:{m_id}:{a}")

        self.created_at = helper.get_value("created_at", casts=(str, float))
        self.message_type = helper.get_value("type")
        self.state = helper.get_value("expectation", casts=(json.dumps, json.loads))


class QuestionValue(MessageValue):
    def __init__(self, helper: "RedisHelper", m_id: int):
        super().__init__(helper, m_id)
        self.expectation_submitted = helper.get_value(
            "expectation_submitted_at", casts=(str, float)
        )
        self.finished_at = helper.get_value("finished_at", casts=(str, float))


class TgUserRedis:
    @staticmethod
    async def users(redis: RedisConnection):
        return map(lambda a: str(a, "utf-8"), await redis.execute("SMEMBERS", "users"))

    @staticmethod
    def _user_key(key):
        return f"{User.get_current().id}:{key}"

    def __init__(self, redis: RedisConnection, chat_id: int = None):
        helper = RedisHelper(
            redis, TgUserRedis._user_key if not chat_id else lambda k: f"{chat_id}:{k}"
        )
        self.chat_id = chat_id
        self.selected_question_groups = helper.get_set("qgroups")
        self.asked_questions = helper.get_set("questions")
        self.notifications = helper.get_set("notifications")
        self.message = lambda m_id: MessageValue(helper, m_id)
        self.question = lambda m_id: QuestionValue(helper, m_id)

        self.flag_no_questions = helper.get_value("no_questions")
        """ If not set, user will get new portion of questions and we'll get rid of old ones"""

        self.flag_no_unfinished_remainders = helper.get_value("unfinished_remainders")
        """ If not set, user will be reminded about their unfinished questions """

    async def delete_message(self, id):
        q = self.message(id)
        await q.state.set(None)
        await q.created_at.set(None)
        await q.message_type.set(None)
        await self.asked_questions.remove(id)

    async def get_user_next_update(self):
        times = await gather(
            self.flag_no_questions.ttl(), self.flag_no_unfinished_remainders.ttl()
        )
        ttu = min(a for a in times if a is not None)
        if ttu is None:
            log.error(
                f"{self.chat_id} is returning None as Time To Update. That indicates an invalid state. "
                f"Returning 30 as a failsafe."
            )
            return 30
        return ttu

    async def get_question_states(self):
        states = []
        for quid in await self.asked_questions.items():
            message = self.message(quid)
            states.append((await message.message_type.get(), await message.state.get()))
        return states

    async def delete_user(self):
        """Deletes all the user associated info"""
        # TODO
        pass
