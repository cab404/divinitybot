#  This file is part of DivinityBot.
#
#  DivinityBot is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  DivinityBot is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with DivinityBot.  If not, see <https://www.gnu.org/licenses/>.

from typing import NamedTuple, TypeVar, Generic, Union, Dict, Callable, Awaitable, Tuple

import typing
from aiogram import Bot, types
from aiogram.types import InlineQuery, CallbackQuery, base
from aiogram.types import Message


class MessageText(NamedTuple):
    text: str
    parse_mode: str = "Markdown"


_St = TypeVar("_St")


class HiddenQuestionState(NamedTuple, Generic[_St]):
    created_at: int
    state: _St


class CreateMessageRequest(NamedTuple):
    bot: Bot
    chat_id: int


class QuestionCallbackQuery(NamedTuple, Generic[_St]):
    query: CallbackQuery
    state: "_St"


class CreateMessageParams(NamedTuple):
    """Basically a copypasta of aiograms send_message params."""

    chat_id: typing.Union[base.Integer, base.String, None] = None
    text: typing.Optional[base.String] = None
    parse_mode: typing.Optional[base.String] = None
    disable_web_page_preview: typing.Optional[base.Boolean] = None
    disable_notification: typing.Optional[base.Boolean] = None
    reply_to_message_id: typing.Optional[base.Integer] = None
    reply_markup: typing.Union[
        types.InlineKeyboardMarkup,
        types.ReplyKeyboardMarkup,
        types.ReplyKeyboardRemove,
        types.ForceReply,
        None,
    ] = None

    def edit(self, message: Message):
        if message.text != self.text or message.reply_markup != self.reply_markup:
            return message.edit_text(
                text=self.text,
                parse_mode=self.parse_mode,
                disable_web_page_preview=self.disable_web_page_preview,
                reply_markup=self.reply_markup,
            )


class Question(NamedTuple, Generic[_St]):
    create_state: "Callable[[], _St]"
    """Can be non-deterministic."""

    render_message: "Callable[[_St], Awaitable[CreateMessageParams]]"
    """Can potentially use async templates, so is an async method"""

    on_inline_query: "Callable[[QuestionCallbackQuery[_St]], Awaitable[_St]]"
    """
    Called whenever user mashes on the questions buttons.
    Can do extra querying to make decisions, so it is async.
    Returns new state.
    """

    is_finished: "Callable[[_St], bool]"
    """
    Returns whether the question has ended, and framework can make calls to 'get_score'.
    Cannot do any additional querying and should be deterministic and pure.
    Should be deterministic and pure.
    """

    is_expectation_submitted: "Callable[[_St], bool]"
    """
    Returns whether expectation has been submitted.
    Should be deterministic and pure.
    """

    is_correct: "Callable[[_St], bool]"
    """
    Returns score for the given state. State guaranteed to satisfy 'is_finished' predicate.
    Should be deterministic and pure.
    """
    get_certainty: "Callable[[_St], float]"
    """
    Returns initial certainty for the given state. State guaranteed to satisfy 'is_expectation_submitted' predicate.
    Should be deterministic and pure.
    """


class QuestionSet(NamedTuple):
    title: str
    questions: Dict[str, Question]


class QuestionConfig(NamedTuple):
    questions_sets: Dict[str, QuestionSet]
