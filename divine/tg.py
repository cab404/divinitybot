#  This file is part of DivinityBot.
#
#  DivinityBot is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  DivinityBot is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with DivinityBot.  If not, see <https://www.gnu.org/licenses/>.
import asyncio
import logging
import random
import time
from asyncio import gather, Queue
from typing import NamedTuple, Union, Tuple, List
from urllib.parse import urlparse

import aiogram
import aioredis
from aiogram.dispatcher.filters import Command
from aiogram.types import (
    Message,
    InlineKeyboardMarkup,
    InlineKeyboardButton,
    CallbackQuery,
)
from aiohttp import BasicAuth

from divine.tg_models import QuestionConfig, QuestionCallbackQuery, Question
from divine.tg_redis import TgUserRedis
from divine.utils import read_template, merge, brier

from concurrent.futures import TimeoutError as TErr

log = logging.getLogger("telegram_bot")


class StorageConfig(NamedTuple):
    redis_params: dict


class TelegramConfig(NamedTuple):
    token: str
    questions: QuestionConfig
    storage: StorageConfig
    proxy: Union[str, None] = None


def parse_auth(proxy_url: str):
    parsed = urlparse(proxy_url)
    if parsed.username is not None:
        return BasicAuth(parsed.username, parsed.password)
    else:
        return None


async def create_telegram_bot(c: TelegramConfig):
    welcome_template = await read_template("templates/hello.md")
    selector_template = await read_template("templates/selector.md")
    alright_template = await read_template("templates/selection_complete.md")

    redis = await aioredis.create_connection(**c.storage.redis_params)

    all_questions = {}
    for _, qs in c.questions.questions_sets.items():
        for quid, q in qs.questions.items():
            if quid in all_questions:
                raise BaseException(f"Question with id {quid} appears more that once!")
            else:
                all_questions[quid] = q
                log.debug(f"{quid} defined.")

    bot = aiogram.Bot(
        c.token,
        **(
            {}
            if c.proxy is None
            else {"proxy": c.proxy, "proxy_auth": parse_auth(c.proxy)}
        ),
    )

    dsp = aiogram.Dispatcher(bot)

    cron_poking_stick = Queue()

    def poke(fr: str = "unnamed"):
        log.debug(f"Poked update cycle from {fr}")
        cron_poking_stick.put(1)

    async def start_message_render_keyboard(chat_id: int):
        rds = TgUserRedis(redis, chat_id=chat_id)

        selected = rds.selected_question_groups
        return InlineKeyboardMarkup(
            inline_keyboard=[
                [
                    InlineKeyboardButton(
                        text=f"✅ {group.title}"  # not selected
                        if await selected.contains(key)
                        else f" {group.title}",  # selected
                        callback_data=key,
                    )
                    for (key, group) in c.questions.questions_sets.items()
                ],
                [InlineKeyboardButton(text="-- OK --", callback_data="__ok")],
            ]
        )

    async def send_question(quid: str, chat_id: int):
        rds = TgUserRedis(redis, chat_id=chat_id)
        q = all_questions[quid]
        state = q.create_state()

        message_params = await q.render_message(state)
        message = await bot.send_message(**merge(message_params, chat_id=chat_id))
        message_id = message.message_id

        msg = rds.message(message_id)
        # setting message type to correct one
        await msg.message_type.set(quid)
        # adding question to list of user questions
        await rds.asked_questions.add(message_id)
        # setting time of creation
        await msg.created_at.set(time.time())
        # saving question state(s)
        await msg.state.set(state)

    async def new_questions(chat_id=None):
        """
            Sends new questions to current user.
        """
        rds = TgUserRedis(redis, chat_id=chat_id)

        # deleting old questions
        async def q__(quid: int):
            msg = rds.message(quid)
            type = await msg.message_type.get()
            if type in all_questions:
                if not all_questions[type].is_finished(await msg.state.get()):
                    await gather(
                        rds.delete_message(quid), bot.delete_message(chat_id, quid)
                    )

        await gather(*(q__(quid) for quid in await rds.asked_questions.items()))

        questions = await random_questions(rds, count=3)

        # sending those
        await gather(
            *(send_question(question_id, chat_id) for question_id, q in questions)
        )
        # deleting notifications

        await gather(
            *(
                gather(
                    bot.delete_message(chat_id, notification_id),
                    rds.delete_message(notification_id),
                    rds.notifications.remove(notification_id),
                )
                for notification_id in await rds.notifications.items()
            )
        )

        # setting flags
        hour = 3600
        send_questions_period = 24 * hour
        reminders_delay = 12 * hour
        await gather(
            rds.flag_no_questions.set_timeout(1, send_questions_period),
            rds.flag_no_unfinished_remainders.set_timeout(1, reminders_delay),
        )

    async def random_questions(rds, count: int) -> List[Tuple[str, Question]]:
        pool = list()
        for set_id in await rds.selected_question_groups.items():
            pool = pool + list(c.questions.questions_sets[set_id].questions.items())
        random.shuffle(pool)
        questions = pool[:count]
        return questions

    @dsp.message_handler(Command("start"))
    async def on_start(header: Message):
        rds = TgUserRedis(redis, chat_id=header.chat.id)

        await redis.execute("SADD", "users", header.chat.id)

        # sending all the available groups
        await bot.send_message(
            chat_id=header.chat.id,
            text=await welcome_template.render_async(),
            parse_mode="Markdown",
            disable_web_page_preview=True,
        )
        selector = await bot.send_message(
            chat_id=header.chat.id,
            text=await selector_template.render_async(),
            parse_mode="Markdown",
            disable_web_page_preview=True,
            reply_markup=await start_message_render_keyboard(header.chat.id),
        )

        await rds.message(selector.message_id).message_type.set("__intro")

    @dsp.message_handler(Command("stats"))
    async def on_results(message: Message):
        await get_user_stats(message.chat.id)

    @dsp.callback_query_handler()
    async def iq_handler(query: CallbackQuery):
        rds = TgUserRedis(redis, chat_id=query.message.chat.id)

        # lets check if this message is a question
        message_id = query.message.message_id
        msg_type = await rds.message(message_id).message_type.get()
        q_data = query.data
        log.info(
            f"Got an inline query: {query.data}, distinguished message type: {msg_type}"
        )

        # # #
        # # #  Handling intro message and stuff.
        # # #

        if msg_type == "__intro":

            selected = rds.selected_question_groups
            # parsing selection of question groups here.
            if q_data == "__ok":

                # we are finished, sending first batch of answers.
                await query.message.delete()
                await bot.send_message(
                    query.message.chat.id, await alright_template.render_async()
                )
                await new_questions(query.message.chat.id)

            else:

                if await selected.contains(q_data):
                    await selected.remove(q_data)
                else:
                    await selected.add(q_data)
                await query.message.edit_reply_markup(
                    await start_message_render_keyboard(query.message.chat.id)
                )

            await query.answer()

        # # #
        # # #  Handling notifications
        # # #

        elif msg_type == "__notification":
            if q_data == "ok":
                await rds.delete_message(query.message.message_id)
                await query.message.delete()

        # # #
        # # #  Handling questions
        # # #

        elif msg_type in all_questions:

            question = all_questions[msg_type]

            exp_state = rds.message(message_id).state

            # Some control sequences

            if query.data == "__ok":
                await query.message.delete()
                return
            if query.data == "__another":
                ((quid, _),) = await random_questions(rds, 1)
                await gather(
                    query.message.delete(), send_question(quid, query.message.chat.id)
                )
                return

            state = await exp_state.get()
            state = await question.on_inline_query(QuestionCallbackQuery(query, state))

            log.info(f"New state for {message_id}: {state}")
            await exp_state.set(state)

            # now checking whether that solved our question.

            if question.is_finished(state):
                log.info(f"Final answer submitted: {message_id}: {state}")
                await rds.question(message_id).finished_at.set(time.time())
                await query.message.edit_text(
                    text="Ответ принят!",
                    reply_markup=InlineKeyboardMarkup(
                        inline_keyboard=[
                            [
                                InlineKeyboardButton(callback_data="__ok", text="OK"),
                                InlineKeyboardButton(
                                    callback_data="__another", text="Ещё один!"
                                ),
                            ]
                        ]
                    ),
                )
                await query.answer("Ответ принят!")
            else:
                if question.is_expectation_submitted(state):
                    log.info(f"Expectation submitted: {message_id}: {state}")
                    await rds.question(message_id).expectation_submitted.set(
                        time.time()
                    )
                    await query.answer("Предположение сохранено!")

                rendered_message = await question.render_message(state)
                updated_message = await rendered_message.edit(query.message)

    async def get_user_stats(chat_id: int):
        rds = TgUserRedis(redis, chat_id)
        states = await rds.get_question_states()
        results = []
        for (qtype, state) in states:
            if qtype in all_questions:
                q = all_questions[qtype]
                if not q.is_finished(state):
                    continue
                certainty = q.get_certainty(state)
                correctness = q.is_correct(state)
                results.append((certainty, correctness))
        brier_score = brier(results)
        await bot.send_message(
            chat_id,
            f"Количество отвеченных вопросов: {len(results)}\n"
            f"brier: {brier_score:.2}",
        )

    # Simple housekeeping tasks
    # * checking all the unsubmitted questions (if timer is due), and notifying about them
    # * removing old questions and send new ones if timer is due

    async def housekeeping():
        users = await TgUserRedis.users(redis)

        async def per_user(chat_id: int):

            # chat = await bot.get_chat(chat_id)
            rds = TgUserRedis(redis, chat_id)

            # If true, we post notifications and set it to 1.
            post_reminders = not bool(await rds.flag_no_unfinished_remainders.get())

            # If true, we post new questions, delete old ones, and set it to 1 with expiry 24h.
            # Also we set unfinished_reminders to 1 with expiry 12h.
            post_questions = not bool(await rds.flag_no_questions.get())

            if post_questions or post_reminders:
                questions = await rds.asked_questions.items()

                for quid in questions:
                    msg_data = rds.message(quid)
                    qtype = await msg_data.message_type.get()

                    if qtype not in all_questions:
                        continue

                    question = all_questions[qtype]

                    try:
                        state = await msg_data.state.get()
                        toc = await msg_data.created_at.get()
                    except BaseException as e:
                        log.info("Found dead state: " + quid + ", removing.", e)
                        await rds.asked_questions.remove(quid)
                        continue

                    finished = question.is_finished(state)
                    submitted = question.is_expectation_submitted(state)

                    if post_questions and not finished:
                        log.info("Deleting untouched question: " + quid + ".")
                        await rds.delete_message(quid)
                        await bot.delete_message(chat_id, quid)
                        continue

                    if post_reminders and submitted and not finished:
                        log.info(
                            f"Found started, but unfinished question, time={toc} user={chat_id}, message={quid}, type={qtype}, elapsed={time.time() - toc}"
                        )
                        notif = await bot.send_message(
                            chat_id,
                            "Похоже, вы забыли про этот вопрос :(",
                            reply_to_message_id=quid,
                            reply_markup=InlineKeyboardMarkup(
                                inline_keyboard=[
                                    [
                                        InlineKeyboardButton(
                                            callback_data="ok", text="OK"
                                        )
                                    ]
                                ]
                            ),
                        )
                        await rds.message(notif.message_id).message_type.set(
                            "__notification"
                        )
                        await rds.notifications.add(notif.message_id)

                # Posting new questions
                if post_questions:
                    await new_questions(chat_id)

                if post_reminders:
                    await rds.flag_no_unfinished_remainders.set("1")

        await gather(*(per_user(int(chat)) for chat in users))

    async def cron_loop():
        await asyncio.sleep(3)
        while True:
            log.info("Cron job starts.")
            await housekeeping()
            log.info("Cron job finished.")

            # Now we'll find out, how long should we wait.
            users = await TgUserRedis.users(redis)
            all_users = await gather(
                *(
                    TgUserRedis(redis, chat_id=chid).get_user_next_update()
                    for chid in users
                )
            )

            if len(all_users) > 0:
                wait_time = min(all_users)
            else:
                log.info("No users in existence, reverting to 100-second loop.")
                wait_time = 100

            log.info(
                f"Next event in {wait_time // 3600:02}:{wait_time % 3600 // 60:02}:{wait_time % 60:02}"
            )
            try:
                await asyncio.wait_for(cron_poking_stick.get(), timeout=wait_time + 1)
                log.info("Cron loop was prodded, starting housekeeping.")
            except TErr:
                log.info("Timeout has passed, starting planned housekeeping tasks.")
                pass

    await gather(dsp.start_polling(), cron_loop())
