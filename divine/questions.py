#  This file is part of DivinityBot.
#
#  DivinityBot is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  DivinityBot is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with DivinityBot.  If not, see <https://www.gnu.org/licenses/>.

import time
from typing import NamedTuple, Tuple
from typing import Optional

from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

from divine.tg_models import (
    MessageText,
    CreateMessageRequest,
    QuestionCallbackQuery,
    Question,
    CreateMessageParams,
)
from divine.utils import merge

BinaryQuestionState = Tuple[Optional[bool], Optional[bool]]


def binary_question(
    text: MessageText, yes_text: str = "Да", no_text: str = "Нет"
) -> "Question[BinaryQuestionState]":
    is_expectation_submitted = lambda s: s[0] is not None
    is_finished = lambda s: is_expectation_submitted(s) and s[1] is not None

    async def render(r: BinaryQuestionState) -> "CreateMessageParams":
        content = (
            f"«{text.text}»\n" f"**Выполнится ли это предположенне?**"
            if not is_expectation_submitted(r)
            else f"«{text.text}»\n" f"**Выполнилось ли это предположение?**"
        )
        return CreateMessageParams(
            text=content,
            parse_mode=text.parse_mode,
            reply_markup=InlineKeyboardMarkup(
                row_width=2,
                inline_keyboard=[
                    [
                        InlineKeyboardButton(yes_text, callback_data="yes"),
                        InlineKeyboardButton(no_text, callback_data="no"),
                    ]
                ],
            ),
        )

    async def on_inline_query(r: "QuestionCallbackQuery[BinaryQuestionState]"):
        ans = r.query.data == "yes"
        if not is_expectation_submitted(r.state):
            return ans, None
        else:
            return r.state[0], ans

    return Question(
        render_message=render,
        on_inline_query=on_inline_query,
        create_state=lambda: (None, None),
        is_finished=is_finished,
        is_expectation_submitted=is_expectation_submitted,
        is_correct=lambda t: t[0] == t[1],
        get_certainty=lambda t: 0.5,
    )


def new_binary_question(
    text: MessageText, yes_text: str = "Да", no_text: str = "Нет"
) -> "Question[dict]":
    is_expectation_submitted = lambda s: s["state"] == 2
    is_finished = lambda s: s["state"] == 3

    async def render(r: dict) -> "CreateMessageParams":
        state = r["state"]

        if state == 0 or state == 2:
            content = (
                f"«{text.text}»\n" f"**Сбудется ли это предположенне?**"
                if not is_expectation_submitted(r)
                else f"«{text.text}»\n" f"**Сбылось ли это предположение?**"
            )
            return CreateMessageParams(
                text=content,
                parse_mode=text.parse_mode,
                reply_markup=InlineKeyboardMarkup(
                    row_width=2,
                    inline_keyboard=[
                        [
                            InlineKeyboardButton(yes_text, callback_data="yes"),
                            InlineKeyboardButton(no_text, callback_data="no"),
                        ]
                    ],
                ),
            )
        elif state == 1:
            return CreateMessageParams(
                text="""С какой вероятностью оно сбудется?""",
                parse_mode=text.parse_mode,
                reply_markup=InlineKeyboardMarkup(
                    row_width=2,
                    inline_keyboard=[
                        [
                            InlineKeyboardButton(f"{exp}%", callback_data=str(exp))
                            for exp in [50, 80, 90, 99]
                        ]
                    ],
                ),
            )

    async def on_inline_query(r: "QuestionCallbackQuery[dict]"):
        state = r.state["state"]
        if state == 0 or state == 2:
            ans = r.query.data == "yes"
            if not is_expectation_submitted(r.state):
                return merge(r.state, expected=ans, state=1)
            else:
                return merge(r.state, actual=ans, state=3)
        elif state == 1:
            return merge(r.state, certainty=int(r.query.data) / 100.0, state=2)

    return Question(
        render_message=render,
        on_inline_query=on_inline_query,
        create_state=lambda: {"state": 0},
        is_finished=is_finished,
        is_expectation_submitted=is_expectation_submitted,
        is_correct=lambda t: t["expected"] == t["actual"],
        get_certainty=lambda t: t["certainty"],
    )
