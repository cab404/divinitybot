#  This file is part of DivinityBot.
#
#  DivinityBot is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  DivinityBot is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with DivinityBot.  If not, see <https://www.gnu.org/licenses/>.
import asyncio
import os
import unittest
import random
from asyncio import gather

from aioredis import create_connection

from divine.tg_redis import TgUserRedis, RedisSet, RedisValue


def sync(f):
    async def subwrapper(*args, **kwargs):
        # that is wonky af, but should work for now
        host = os.getenv("REDIS_TEST_HOST", "localhost")
        redis = await create_connection(address=(host, 6379))
        await f(*args, redis, **kwargs)

    def wrapper(*args, **kwargs):
        asyncio.run(subwrapper(*args, **kwargs))

    return wrapper


class TestRedisBindings(unittest.TestCase):
    @sync
    async def test_sets(self, redis):
        t = RedisSet(redis, "testcases-set")

        async def clean():
            await gather(*(t.remove(n) for n in await t.items()))

            self.assertEqual(set(), await t.items())

        await clean()
        nums = {str(random.randint(1, 100)) for _ in range(10)}
        await gather(*(t.add(n) for n in nums))

        self.assertEqual(nums, await t.items())

        await clean()

    @sync
    async def test_values(self, redis):
        t = RedisValue(redis, "testcases-value", (lambda a: a[0], lambda a: (a,)))

        val = ("Some test value",)
        await t.set(val)
        self.assertEqual(val, await t.get())

        await t.set_timeout(val, 1)
        self.assertEqual(val, await t.get())
        await asyncio.sleep(1.1)
        self.assertEqual((None,), await t.get())

    @sync
    async def test_user_updates(self, redis):
        t = TgUserRedis(redis, 1)

        await t.flag_no_unfinished_remainders.set_timeout("1", 1000)
        await t.flag_no_questions.set_timeout("1", 700)
        timeout = await t.get_user_next_update()
        self.assertAlmostEqual(70, timeout / 10, 1)

        await t.flag_no_unfinished_remainders.set("1")
        await t.flag_no_questions.set_timeout("1", 1000)
        timeout = await t.get_user_next_update()
        self.assertAlmostEqual(100, timeout / 10, 1)

        await t.flag_no_unfinished_remainders.set(None)
        await t.flag_no_questions.set_timeout("1", 100)
        timeout = await t.get_user_next_update()
        self.assertEqual(0, timeout, 1)
